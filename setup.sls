setup:
  pkg.installed:
    - pkgs:
      - git
      - haveged
      - htop
      - mosh
      - net-tools
      - p7zip
      - python
      - python2
      - python-cherrypy
      - python2-cherrypy
      - python-gitpython
      - python2-gitpython
      - python-pip
      - python2-pip
      - python-dateutil
      - python2-dateutil
      - python-docker
      - python2-docker
      - screen  # For long-running tasks I may need to disconnect during
      - sudo

      - {{ pillar['editor'] }}  # Editor (eg, nano)

  user.present:
    - name: gord
    - home: /home/gord

  timezone.system:
    - name: America/Denver

update:
  pkg.uptodate:
    - name: "Update"
    - refresh: True

