frognation-pull:
  module.run:
    - name: dockercompose.pull
    - path: /srv/compose/frognation

frognation:
  environ.setenv:
    - value:
        CLIENT_ID: "{{ pillar["env"]["FROGNATION_CLIENT_ID"] }}"
        CLIENT_SECRET: "{{ pillar["env"]["FROGNATION_CLIENT_ID"]}}"
        TOKEN: "{{pillar["env"]["FROGNATION_TOKEN"]}}"

  module.run:
    - name: dockercompose.up
    - path: /srv/compose/frognation
