pkmngots-pull:
  module.run:
    - name: dockercompose.pull
    - path: /srv/compose/pkmngots

pkmngots:
  module.run:
    - name: dockercompose.up
    - path: /srv/compose/pkmngots
