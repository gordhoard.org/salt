hoardbot-pull:
  module.run:
    - name: dockercompose.pull
    - path: /srv/compose/hoardbot

hoardbot:
  environ.setenv:
    - value:
        TOKEN: "{{pillar["env"]["HOARDBOT_TOKEN"]}}"
        WOLFRAM_TOKEN: "{{pillar["env"]["HOARDBOT_WOLFRAM"]}}"

  module.run:
    - name: dockercompose.up
    - path: /srv/compose/hoardbot
