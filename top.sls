base:
  "*":
    - setup

    - init/keys
    - init/locales

    - init/docker
    - init/iptables
    - init/haveged
    - init/nginx/all
    - init/sshd
    - init/sudo

  "dedicated.gordhoard.org":
    - init/nginx/dedicated

    - docker/frognation
    - docker/hoardbot
    - docker/pkmngots

    - iptables/save  # So we have the tables again when the system restarts. Arch always reloads them there.
