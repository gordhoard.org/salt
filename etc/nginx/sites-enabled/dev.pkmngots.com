server {
  listen 80;

  server_name dev.pkmngots.com;

  location / {
      return 301 https://$server_name$request_uri;
  }

  location ~* /\.well-known/acme-challenge/.* {
    allow all;
    root /srv/acme;
    try_files $uri =404;
  }
}

server {
  listen 443 ssl http2;
  server_name dev.pkmngots.com;

  ssl_certificate /etc/letsencrypt/live/dev.pkmngots.com/fullchain.pem;
  ssl_certificate_key /etc/letsencrypt/live/dev.pkmngots.com/privkey.pem;

  location / {
		proxy_pass http://localhost:43345/;
		proxy_set_header Host $host;
		proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
	}
  client_max_body_size 100M;
}
