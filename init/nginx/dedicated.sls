init_nginx:
  pkg.installed:
    - pkgs:
      - nginx

  service.running:
    - name: nginx

    - enable: True
    - reload: True

    - watch:
      - file: init_nginx_conf
      - file: init_nginx_files
      - file: init_letsencrypt

      - acme: dedicated.gordhoard.org
      - acme: api.gordhoard.org
      - acme: frognation.info
      - acme: dev.pkmngots.com

init_nginx_conf:
  file.managed:
    - name: /etc/nginx/nginx.conf
    - source: salt://etc/nginx/nginx.conf

init_nginx_files:
  file.recurse:
    - name: /etc/nginx/sites-enabled
    - source: salt://etc/nginx/sites-enabled/

init_letsencrypt:
  file.managed:
    - name: /etc/letsencrypt/cli.ini
    - source: salt://etc/letsencrypt/cli.ini

# SSL certs
dedicated.gordhoard.org:
  acme.cert:
    - email: izac@gordhoard.org
    - webroot: /srv/acme
    - renew: 14
    - certname: dedicated.gordhoard.org

api.gordhoard.org:
  acme.cert:
    - email: izac@gordhoard.org
    - webroot: /srv/acme
    - renew: 14
    - certname: api.gordhoard.org

frognation.info:
  acme.cert:
    - email: izac@gordhoard.org
    - webroot: /srv/acme
    - renew: 14
    - certname: frognation.info

dev.pkmngots.com:
  acme.cert:
    - email: izac@gordhoard.org
    - webroot: /srv/acme
    - renew: 14
    - certname: dev.pkmngots.com
