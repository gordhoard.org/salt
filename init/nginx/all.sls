iptables_http:
  iptables.append:
    - chain: INPUT
    - protocol: tcp
    - dport: 80
    - jump: ACCEPT

iptables_https:
  iptables.append:
    - chain: INPUT
    - protocol: tcp
    - dport: 443
    - jump: ACCEPT
