init_iptables:
  pkg.installed:
    - pkgs:
      - iptables

  service.running:
    - name: iptables
    - enable: true
